package com.pocketdesk;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
//import com.facebook.reactnative.androidsdk.FBSDKPackage;
import im.shimo.react.prompt.RNPromptPackage;
import com.rn.full.screen.FullScreenModule;
import com.evollu.react.fcm.FIRMessagingPackage;
import com.audioStreaming.ReactNativeAudioStreamingPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.github.yamill.orientation.OrientationPackage;
import com.corbt.keepawake.KCKeepAwakePackage;
//import im.shimo.react.prompt.RNPromptPackage;
//import com.wog.videoplayer.VideoPlayerPackage;
//import com.image.zoom.ReactImageZoom;
//import com.rn.full.screen.FullScreenModule;
//import com.sh3rawi.RNAudioPlayer.RNAudioPlayer;
//import com.audioStreaming.ReactNativeAudioStreamingPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.brentvatne.react.ReactVideoPackage;
// import com.calendarevents.CalendarEventsPackage;
import com.reactlibrary.RNReactNativeDocViewerPackage;
// import com.wog.videoplayer.VideoPlayerPackage;
import com.RNPlayAudio.RNPlayAudioPackage;

import com.learnium.RNDeviceInfo.RNDeviceInfo;

import com.rt2zz.reactnativecontacts.ReactNativeContacts;
import in.sriraman.sharedpreferences.RNSharedPreferencesReactPackage;
// import com.vonovak.AddCalendarEventPackage;
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;
import com.reactnativedocumentpicker.ReactNativeDocumentPicker;
import com.imagepicker.ImagePickerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.facebook.FacebookSdk;
import com.facebook.CallbackManager;
import com.facebook.appevents.AppEventsLogger;
import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

  protected static CallbackManager getCallbackManager() {
    return mCallbackManager;
  }

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
           // new FBSDKPackage(),
            new RNPromptPackage(),
            new FullScreenModule(),
            new FIRMessagingPackage(),
            new ReactNativeAudioStreamingPackage(),
            new RNFetchBlobPackage(),
            new LinearGradientPackage(),
            new OrientationPackage(),
            new KCKeepAwakePackage(),
           // new RNPromptPackage(),
           // new VideoPlayerPackage(),
            //new ReactImageZoom(),
            //new FullScreenModule(),
           // new RNAudioPlayer(),
            //new ReactNativeAudioStreamingPackage(),
            new VectorIconsPackage(),
            new ReactVideoPackage(),
            // new CalendarEventsPackage(),
            new RNReactNativeDocViewerPackage(),
            // new VideoPlayerPackage(),
            new RNPlayAudioPackage(),
           // new RNGooglePlacesPackage(),
            new RNDeviceInfo(),
            //new BackgroundTaskPackage(),
            new ReactNativeContacts(),
            new RNSharedPreferencesReactPackage(),
            // new AddCalendarEventPackage(),
            new ReactNativePushNotificationPackage(),
            new ReactNativeDocumentPicker(),
            new ImagePickerPackage(),
              //**  ADD THE FOLLOWING LINE **//
           new FBSDKPackage(mCallbackManager)
      );
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
    
  }
}
