var express = require('express');
var router = express.Router();
var deskPostModel = require('../models/deskPostModel');
var Post = require('../models/postmodel');
var CommentModel = require('../models/commentmodel');
var Network = require('../models/networkmodel');
var request = require('request');

router.route('/getDeskPost')
.post(function (req, res) {

  deskPostModel.getDeskPostList({user_id : req.body.user_id }, function (error, deskPostList) {
      if (error)
          return res.send({ status: 'false', message: error});

    //  return res.json(deskPostList);
      if(deskPostList.length > 0){
        var loopstill = (deskPostList.length) - 1;
        var Jsonresult = [];
        console.log(deskPostList);
         deskPostList.forEach( function(list,k) {

           if(list.desk_type == "1"){
             console.log('type1');
           Post.getPostForDesk( list.post_id , function (error, postList) {
            if (error)
                return res.json({ status : 'false', message : err });
              if(postList.length > 0){

                    Jsonresult[k] = postList;

                    Post.getTotallikse( { $and : [  { isPostLike : "1" }, {  post_id : postList[0]._id } ] }, function (err, totalLikse) {
                        if (!err)
                        Jsonresult[k][0]['totalLikse'] = totalLikse.length;
                        Jsonresult[k][0]['deskPostDateTime'] = list.deskPost_createdDate;
                        Jsonresult[k][0]['deskPostId'] = list._id;
                        Jsonresult[k][0]['desk_type'] = "1";

                        Post.isUserLiked( { $and : [  { isPostLike : "1" }, {  post_id : postList[0]._id }, {  user_id : req.body.user_id } ] }, function (err, isLikeOrNot) {
                            if (!err)
                            if(isLikeOrNot.length > 0)
                              Jsonresult[k][0]['isLike'] = '1';
                            else
                              Jsonresult[k][0]['isLike'] = '0';

                              CommentModel.getComment(postList[0]._id, function (error, commentDetail) {
                                if (!error)
                                    Jsonresult[k][0]['comment'] = commentDetail;

                                    if (k == loopstill)
                                      return res.send({ status: 'true', message: "Desk post list found!", post : Jsonresult });

                              });

                        });

                    });
            //    });
             } else {
                return res.send({ status: 'true', message: "Post not found!" });
             }
          });
        }else{
          console.log('type2');
          Network.getSingleLocation( list.network_id, list.location_id, function (error, locationData) {
              if (error)
                  return res.send({ status: 'false', message: error});
                  
                  Jsonresult[k] = locationData;

                  if (k == loopstill)
                    return res.send({ status: 'true', message: "Desk post list found!", post : Jsonresult });
          });
        }
      });
   }else{
     return res.send({ status: 'true', message: "Post not found in desk!" });
   }
  });
});

router.route('/saveAsDesk')
.post(function (req, res) {
   console.log(req.body);
    deskPostModel.alreadyDesk({ network_id : req.body.network_id, post_id : req.body.post_id, user_id : req.body.user_id }, function (err, postDeskDetail) {
        if(postDeskDetail.length > 0){
            return res.send({ status: 'false', message: "You already desk this post!"});
        }else{
          // return res.json(req.body);
            deskPostModel.saveAsDesk(req.body, function (err, list) {
                if (err)
                    res.json(err);
                else {
                    return res.send({ status: 'true', message: "post added in desk list successfully!", data : list });
                }
            });
        }
    });
});


router.route('/removeDeskPost')
.post(function (req, res) {
    deskPostModel.removeDeskPost( { _id : req.body._id }, function (error, network) {
        if (error)
            return res.send({ status: 'false', message: error});
          if(network != null)
            return res.send({ status: 'true', message: "Desk post removed successfully!" });
          else
            return res.send({ status: 'false', message: "Desk post not remove!"});
    });
});

module.exports = router;
