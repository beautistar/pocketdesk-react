var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;
var deskPostSchema = new mongoose.Schema({
    network_id: { type: ObjectId },
    post_id: { type: ObjectId },
    location_id: { type: String },
    user_id: { type: ObjectId,  required: true },
    desk_type: { type: String, default: 1 },  // for post
    deskPost_createdDate: { type: Date, default: Date.now },
});

var deskPost = module.exports = mongoose.model('deskPost', deskPostSchema);

// desk post list
module.exports.getDeskPostList = function (query, callback) {
    deskPost.find(query, callback).sort({ _id : -1 });
}

// add desk post
module.exports.saveAsDesk = function (postDesk, callback) {
    deskPost.create(postDesk, callback);
}

// check post already desk or not
module.exports.alreadyDesk = function (query, callback) {
    deskPost.find(query, callback);
}

// check user desk this post or not
module.exports.isUserDeskedThisPost = function (query, callback) {
    deskPost.find(query, callback);
}

// delete desk post
module.exports.removeDeskPost = function (query, callback) {
    deskPost.remove(query, callback);
}

// save location in desk
module.exports.saveLocationAsDesk = function (locationData, callback) {
    deskPost.create(locationData, callback);
}
