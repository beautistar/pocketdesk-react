import { AppRegistry } from 'react-native';
import React, { Component } from 'react';
import App from './app/components';
import { Provider } from "react-redux";
import store from './app/components/config/store'

export default class PocketDesk extends Component {
  render() {
    return (
    <Provider store={store}>
        <App/>
     </Provider>
    );
  }
}
AppRegistry.registerComponent('PocketDesk', () => PocketDesk);